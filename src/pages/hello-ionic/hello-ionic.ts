
import { Component, EventEmitter, Injectable } from '@angular/core';
import { GetClockTime } from "./clock.models";
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
declare var $: any;

@Injectable()

@Component({
  selector: 'page-hello-ionic',
  templateUrl: 'hello-ionic.html'
})
export class HelloIonicPage {
  // Declare the variables
  private proxy: any;
  private proxyName: string = 'listingHub';
  public request: any;
  public signalrAuctionJsonRequest: any;
  public signalrBuyNowJsonRequest: any;
  public abc:string;
  //private hubCreated: any ;

  private connection: any;
  // private demodata:any;
  // create the Event Emitter
  public messageReceived: EventEmitter<GetClockTime>;
  public connectionEstablished: EventEmitter<Boolean>;
  public connectionExists: Boolean;
  constructor(public http: Http) {
    this.messageReceived = new EventEmitter<GetClockTime>();
    // let header = new Headers();
    // header.append("Access-Control-Allow-Origin","*");
    // header.append("content-type","application/json");
    // debugger;
    // Constructor initializçation
    this.connectionEstablished = new EventEmitter<Boolean>();
    //  this.messageReceived = new EventEmitter < GetClockTime > ();
    this.connectionExists = false;
    // create hub connection
    //   $.connection.hub.transportConnectTimeout = 3000;
    this.connection = $.hubConnection('http://192.168.2.44/awdemo/');

    //  console.log(this.connection.proxies.listinghub);
    // create new proxy as name already given in top
    this.proxy = this.connection.createHubProxy(this.proxyName);
    // register on server events

   //  this.registerOnServerEvents();

  // this.proxy.client.setRealTime = function (data: GetClockTime) {
  //             console.log('received in SignalRService: ' + JSON.stringify(data));
  //           }



    this.request= {
  "CultureUIName": "en",
  "CultureName": "en",
  "FBOUserName": "dhanashri",
  //"FBOUserName": "shruti",
  "ActingUserName": "dhanashri",
 // "ActingUserName": "shruti",
  "Items": {
    "ListingID": "164368",
    "ListingType": "FixedPrice",
    "BidAmount": "1100",
     "Quantity":1
  },
  "Raw": null
};

this.signalrAuctionJsonRequest= {
  "AcceptedActionCount": 1,
  "CurrencyCode": "USD",
  "CurrentListingActionUsername": "dhanashri",
  //"CurrentListingActionUsername": "shruti",
  "Increment": 25,
   "ListingID": 162251,
   "NextPrice": 2035,
   "Price": 2010,
   "Quantity": "1",
   "Source": "BID_ORIGIN",
  "Properties": {
    "BuyNowPrice": 0.0,
    "BuyNowStatus": false,
    "ReserveDefined": false,
    "ReserveMet": true,
  },
  "Username": "dhanashri"
  //"Username": "shruti"
};


this.signalrBuyNowJsonRequest= {
  "AcceptedActionCount": 0,
  "CurrencyCode": "USD",
  "CurrentListingActionUsername": "dhanashri",
  "Increment": "0",
   "ListingID": "164368",
   "NextPrice": "1100",
   "Price": "1100",
   "Quantity": "1",
   "Source": "BUY_ORIGIN",
  "Properties": null,
  "Username": "dhanashri"
};
this.abc =JSON.stringify(this.signalrBuyNowJsonRequest);
//this.abc =JSON.stringify(this.signalrAuctionJsonRequest);
    // call the connecion start method to start the connection to send and receive events.
    this.startConnection();


    this.registerOnServerEvents();


//this.changePasswordService();
// this.doLogin().subscribe(res=>{ console.log("res",res);});
    //  this.signalrAuctionNotify("162251","ListingActionChange",this.signalrAuctionJsonRequest);


//this.doLogin();
  }

  // check in the browser console for either signalr connected or not
  private startConnection(): void {
    //  debugger;
    this.connection.start().done((data: any) => {
      console.log('Now connected ' + data.transport.name + ', connection ID= ' + data.id);
      this.connectionEstablished.emit(true);
      this.connectionExists = true;




     // this.RegisterUserName("shruti");
      this.RegisterUserName("dhanashri");
      this.RegisterListing(162251);
//this.signalrAuctionNotify("162251","ListingActionChange",this.signalrAuctionJsonRequest);

     // this.signalrAuctionNotify("162251","ListingActionChange",this.signalrAuctionJsonRequest);
      this.ListingActionNotify();
       this.proxy.invoke('GetRealTime').then((data?: any) => {
      console.log("Registered Listing");
    });

     //console.log("FireTestMessage this.abc",this.abc);

    // this.proxy.invoke('FireTestMessage',162251,"ListingActionChange",this.abc).then((data: any) => {
    //  console.log("FireTestMessage called");
    // });


this.doLogin().subscribe(res => {
     console.log("res",res);
     this.proxy.invoke('FireTestMessage',162251,"ListingActionChange",this.abc).then((data: any) => {
     console.log("FireTestMessage called");
    });
    }, error => {
      console.log(error);
      }
    );


    }).fail((error: any) => {
      console.log('Could not connect ' + error);
      this.connectionEstablished.emit(false);
    });
  }

  private RegisterUserName(Name: string) {
    console.log('Going inside RegisterUserName ' + Name);
    //$.connection.listingHub.server.registerUserName(Name);
    //this.proxy.RegisterUserName(Name);
    //   this.demodata=this.proxy.invoke('RegisterUserName', "admin");

    this.proxy.invoke('RegisterUserName', Name).then((data: any) => {
      console.log("Registered username", Name);
    });
    //  this.proxy.on('FireTestMessage',"162251","ListingActionChange",this.signalrAuctionJsonRequest, () => {
    //         console.log('FireTestMessage signalr called');
    //     });
  }



  private RegisterListing(ListingId: number) {
    console.log('Going inside  RegisterListing' + ListingId);
    this.proxy.invoke('RegisterListingInterest', ListingId).then((data: any) => {
      console.log("Registered Listing", ListingId);
    });


//  this.proxy.on('setRealTime', (data: GetClockTime) => {
//             console.log('received in SignalRService: ' + JSON.stringify(data));
//             console.log('hellow received in SignalRService:');
//             //this.messageReceived.emit(data);
//         });
//this.registerOnServerEvents();

  }

  private ListingActionNotify() {
    console.log('Going inside ListingActionNotify');


    this.proxy.on('SignalR_UpdateListingAction', (data: GetClockTime) => {
      console.log('SignalR_UpdateListingAction');
      // this.messageReceived.emit(data);
    });
  }

  // public changePasswordService(){
  //   console.log("request")
  //   let headers = new Headers({ 'Authorization': 'RWX_BASIC admin:admin1234','Content-Type':'Application/json' });
  //   let options = new RequestOptions({ headers: headers, body: this.request });

  //   //let url = "https://awdemo.systenics.com/api/listingaction";
  //   let url = "https://192.168.2.44/awdemo/api/listingaction";

  //   this.http.post(url, options)
  //     .map((res: Response) => console.log("hii",res)) // ...and calling .json() on the response to return data
  //     .catch((error: any) => Observable.throw(error.json().error || 'Server error'));

  // }
doLogin():Observable<string> {
  console.log("service call");
     let bodyString = JSON.stringify(this.request); // Stringify payload
        let headers      = new Headers({ 'Authorization': 'RWX_BASIC admin:admin1234','Content-Type':'application/json;charset=utf-8' }); // ... Set content type to JSON
        let options       = new RequestOptions({ headers: headers }); // Create a request option
        return this.http.post("https://192.168.2.44/awdemo/api/listingaction",bodyString, options) // ...using put request
                         .map((res:Response) => console.log("hiifdfd",res)) // ...and calling .json() on the response to return data
                         .catch((error:any) => Observable.throw(error.json().error || 'Server error')); //...errors if

 }


 private registerOnServerEvents(): void {
        this.proxy.on('setRealTime', (data: GetClockTime) => {
            console.log('received in SignalRService: ' + JSON.stringify(data));
            console.log('hellow received in SignalRService:');
            //this.messageReceived.emit(data);
        });
        this.proxy.on('setRealTime1', (data: string) => {
            console.log('Caller in SignalRService: ' + data);
        });
        this.proxy.on('setRealTime12', (data: string) => {
            console.log('Caller in SignalRService: ' + data);
        });

    }

    private signalrAuctionNotify(listingID:string,dataType:string,rawData:string): void {
      console.log('FireTestMessage',listingID,dataType,rawData);
      debugger;
        this.proxy.invoke('FireTestMessage', listingID,dataType,rawData).then((data: any) => {
     console.log("FireTestMessage called");
    });
    }

}
